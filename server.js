//Imports
var express = require('express');
var request = require('request');
var crypto = require('crypto');

//Shared vars
var nums = JSON.parse(process.argv[4]);
var token = process.argv[2];
var wait = process.argv[3];
var shasum = crypto.createHash('sha256');
var pass = shasum.update(token).digest('hex');

console.log("Hashed PW: " + pass);
console.log("Waiting:   " + wait + "s");
console.log("Number(s): " + nums);

var app = express();
var last_ping = new Date(); 

//Main page, description of service and
//Links to front, back and leave a message
app.get('/', function(req, res) {
    res.end('/front for front door and /back for the back.');
});

app.get('/front/:token', function(req, res) {
    var time = new Date();
    var diff = (time - last_ping) / 1000;
    if (diff < wait) {
        res.send(202, "Already knocked. Please wait before knocking again.");
    }
    else {
        if (req.params.token == pass) {
            for (var num in nums) {
                var message = "Knock knock - Front\n" + time;
                var data = { form: { message: message, number:nums[num] } }
                request.post('http://textbelt.com/text', data, message_sent);
                
            }
            last_ping = time;

            res.send(200, 'Front door knocked!');
        }
        else {
            res.send(404, "Sorry, the requested page can not be found.");
        }
    }
});


app.get('/back/:token', function(req, res) {
    var time = new Date();
    
    var diff = (time - last_ping) / 1000;
    if (diff < wait) {
        res.status(202);
        res.end("Already knocked. Please wait before knocking again.");
    }
    if (req.params.token == pass && diff >= wait) {
        for (var num in nums) {
            
            var message = "Knock knock - Back\n" + time;
            var data = { form: { message: message, number:nums[num] } }
            request.post('http://textbelt.com/text', data, message_sent);
        }
        last_ping = time;
        res.status(200)
        res.end('Back door knocked!');
    }
    else {
        res.status(404)
        res.end();
    }
    
});

app.listen(3000);

function message_sent(error, resposne, body) {
    //console.log(resposne);
    console.log("door knocked");
}

